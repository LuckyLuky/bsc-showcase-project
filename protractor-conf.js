exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ["src/spec/**/*.spec.js"],
  onPrepare: () => {
    browser.ignoreSynchronization = true;
  },
  multiCapabilities: [
    {
      browserName: 'firefox'
    }, 
    {
      browserName: 'chrome'
    }
  ]
};