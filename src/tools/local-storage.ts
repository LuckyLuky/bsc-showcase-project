const PREFIX = 'bsc_notie'

const LANGUAGE_KEY = `${PREFIX}_language`

export const saveLanguage = (language: string) => {
  localStorage.setItem(LANGUAGE_KEY, language)
}

export const getLanguage = (): string | null => {
  return localStorage.getItem(LANGUAGE_KEY)
}
