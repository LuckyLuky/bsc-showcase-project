declare module 'react-i18next/hooks' {
  import { TranslationFunction } from 'i18next'
  import i18next = require('i18next')

  export function useTranslation(): [TranslationFunction, i18next.i18n]

  export interface InitReactI18n {
    type: string
    init: (instance: i18next.i18n) => void
  }

  export const initReactI18n: InitReactI18n
}
