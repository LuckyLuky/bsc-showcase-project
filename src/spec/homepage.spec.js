describe('notie homepage', () => {
  beforeEach(() => {
    browser.get('http://localhost:3000/')
  })

  it('should have a title', () => {
    expect(browser.getTitle()).toEqual('Notie')
  })

  it('should have a heading', () => {
    const heading = element.all(by.css('.header__title'))
    expect(heading.count()).toEqual(1)
  })

  it('should have exactly one language selected', () => {
    const activeFlag = element.all(by.css('.language__flag--active'))
    expect(activeFlag.count()).toEqual(1)
  })

  it('should have exactly two languages', () => {
    const activeFlag = element.all(by.css('.language__flag'))
    expect(activeFlag.count()).toEqual(2)
  })

  it('should render notes', () => {
    browser.wait(protractor.until.elementLocated(by.css('.note')), 5000, 'Error: Notes were not loaded within 5 seconds');
  })
});