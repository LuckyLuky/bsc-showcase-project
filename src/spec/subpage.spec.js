describe('notie subpage', () => {
  beforeEach(() => {
    browser.get('http://localhost:3000/new-note/')
  })

  it('heading should redirect to homepage', async () => {
    const heading = element.all(by.css('.header__title'))
    await heading.click()

    browser.driver.wait(async () => {
      const url = await browser.driver.getCurrentUrl()
      return url === 'http://localhost:3000/'
    }, 3000, 'Error: Redirect to homepage failed within 3 seconds')
  })
});