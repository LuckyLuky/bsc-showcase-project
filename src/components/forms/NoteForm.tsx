import * as React from 'react'
import { Note } from '../../models/notes'
import { useTranslation } from 'react-i18next/hooks'

interface NoteFormProps {
  note?: Note | null
  onSubmit: (note: Note) => any
  onCancel: () => any
}

const NoteForm = ({ note, onSubmit, onCancel }: NoteFormProps) => {
  const [t] = useTranslation()

  const [title, setTitle] = React.useState(note ? note.title : '')

  const onTitleChange = (event: React.FormEvent<HTMLTextAreaElement>) => {
    setTitle((event.target as HTMLInputElement).value)
  }

  const onFormSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault()

    onSubmit({
      title,
      id: note ? note.id : 0,
    })
  }

  return (
    <form onSubmit={onFormSubmit} className="card p-5">
      <div className="form-group">
        <label htmlFor="title">{t('noteTitle')}</label>
        <textarea
          required
          name="title"
          className="form-control"
          id="title"
          placeholder={t('enterTitle')}
          onChange={onTitleChange}
          value={title}
        />
      </div>

      <button type="submit" name="submit" className="btn btn-primary mt-4">
        {t('submit')}
      </button>
      <button
        type="button"
        name="cancel"
        className="btn btn-danger mt-2"
        onClick={onCancel}
      >
        {t('cancel')}
      </button>
    </form>
  )
}

export default NoteForm
