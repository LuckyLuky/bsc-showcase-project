import * as React from 'react'
import { ChildrenRenderingComponent } from '..'
import './Description.scss'

interface DescriptionProps extends ChildrenRenderingComponent {}

const Description = ({ children }: DescriptionProps) => (
  <div className="text--description">{children}</div>
)

export default Description
