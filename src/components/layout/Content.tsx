import * as React from 'react'
import { ChildrenRenderingComponent } from '..'
import classNames from 'classnames'

interface ContentProps extends ChildrenRenderingComponent {
  direction?: 'row' | 'column'
  wrap?: boolean
  centered?: boolean
}

const Content = ({
  children,
  wrap,
  centered,
  direction = 'row',
}: ContentProps) => (
  <div
    className={classNames([
      'd-flex',
      'p-3',
      {
        'flex-row': direction === 'row',
        'flex-column': direction === 'column',
        'flex-wrap': wrap,
        'justify-content-center': centered,
      },
    ])}
  >
    {children}
  </div>
)

export default Content
