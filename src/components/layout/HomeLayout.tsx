import * as React from 'react'
import { ChildrenRenderingComponent } from '..'
import Header from '../ui/Header'
import ErrorBar from '../ui/ErrorBar'
import LanguageSwitcher from '../logic/LanguageSwitcher'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import { AppRoutes } from '../../AppRouter'

interface HomeLayoutProps extends ChildrenRenderingComponent {}

const HomeLayout = ({ children }: HomeLayoutProps) => (
  <div className="container-fluid content">
    <Link to={AppRoutes.Default}>
      <Header />
    </Link>

    <main className="container content__container">
      <LanguageSwitcher />

      <ErrorBar />

      {children}
    </main>
  </div>
)

export default withRouter(HomeLayout)
