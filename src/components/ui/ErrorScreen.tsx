import * as React from 'react'
import HomeLayout from '../layout/HomeLayout'
import Content from '../layout/Content'
import DroidsImage from '../../assets/images/droids.jpg'

const ErrorScreen = () => (
  <HomeLayout>
    <Content centered>
      <img src={DroidsImage} />
    </Content>
  </HomeLayout>
)

export default ErrorScreen
