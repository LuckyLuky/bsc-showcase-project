import * as React from 'react'
import { Note } from '../../models/notes'
import NoteControl from '../logic/NoteControl'

interface NotesListProps {
  notes: Note[]
}

const NotesList = ({ notes }: NotesListProps) => {
  return (
    <React.Fragment>
      {notes.map((note: Note) => (
        <NoteControl key={note.id} note={note} />
      ))}
    </React.Fragment>
  )
}

export default NotesList
