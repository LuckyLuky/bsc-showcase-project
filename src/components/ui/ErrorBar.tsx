import * as React from 'react'
import { connect } from 'react-redux'
import { State } from '../../models'

const mapState = (state: State) => ({
  networkErrors: state.errors.networkErrors,
})

type ErrorBarProps = ReturnType<typeof mapState>

const ErrorBar = ({ networkErrors }: ErrorBarProps) => (
  <React.Fragment>
    {networkErrors &&
      networkErrors.map((networkError: string, index: number) => (
        <div key={index} className="alert alert-danger" role="alert">
          {networkError}
        </div>
      ))}
  </React.Fragment>
)

export default connect(mapState)(ErrorBar)
