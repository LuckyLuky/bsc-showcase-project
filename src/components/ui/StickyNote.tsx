import * as React from 'react'
import './StickyNote.scss'
import { ChildrenRenderingComponent } from '..'

interface StickyNoteProps extends ChildrenRenderingComponent {}

const StickyNote = ({ children }: StickyNoteProps) => (
  <div className="note">{children}</div>
)

export default StickyNote
