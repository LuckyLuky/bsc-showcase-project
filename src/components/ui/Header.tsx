import * as React from 'react'
import { useTranslation } from 'react-i18next/hooks'
import './Header.scss'

const Header = () => {
  const [t] = useTranslation()

  return (
    <header className="d-flex header__container">
      <h1 className="header__title">{t('title')}</h1>
    </header>
  )
}

export default Header
