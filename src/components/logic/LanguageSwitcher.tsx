import * as React from 'react'
import { useTranslation } from 'react-i18next/hooks'
import CzechFlag from '../icons/flags/CzechFlag'
import BritishFlag from '../icons/flags/BritishFlag'
import classNames from 'classnames'
import './LanguageSwitcher.scss'
import { saveLanguage } from '../../tools/local-storage'

const LanguageSwitcher = () => {
  const [t, i18n] = useTranslation()

  const changeLanguage = (language: string) => {
    i18n.changeLanguage(language)
    saveLanguage(language)
  }

  const activeLanguage = i18n.language

  return (
    <div className="d-flex flex-row justify-content-end">
      <CzechFlag
        className={classNames([
          'language__flag',
          'ml-1',
          'mr-1',
          {
            'language__flag--active': activeLanguage === 'cs',
          },
        ])}
        onClick={() => changeLanguage('cs')}
      />

      <BritishFlag
        className={classNames([
          'language__flag',
          'ml-1',
          'mr-1',
          {
            'language__flag--active': activeLanguage === 'en',
          },
        ])}
        onClick={() => changeLanguage('en')}
      />
    </div>
  )
}

export default LanguageSwitcher
