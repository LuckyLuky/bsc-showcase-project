import * as React from 'react'
import { Note } from '../../models/notes'
import StickyNote from '../ui/StickyNote'
import NeutralButton from '../buttons/NeutralButton'
import NegativeButton from '../buttons/NegativeButton'
import { useTranslation } from 'react-i18next/hooks'
import Content from '../layout/Content'
import { withRouter, RouteComponentProps } from 'react-router'
import { AppRoutes } from '../../AppRouter'
import { sendDeleteNoteRequest } from '../../api/notes'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { setNetworkErrors } from '../../models/errors'

const mapDispatch = (dispatch: Dispatch) => ({
  setError: (error: string) => dispatch(setNetworkErrors([error])),
})

interface NoteControlOwnProps {
  note: Note
}

type NoteControlProps = NoteControlOwnProps &
  RouteComponentProps &
  ReturnType<typeof mapDispatch>

const NoteControl = ({ note, history, setError }: NoteControlProps) => {
  const [t] = useTranslation()

  const showNote = () => {
    history.push(`${AppRoutes.NoteDetail}/${note.id}`)
  }

  const deleteNote = () => {
    if (confirm(t('deleteConfirm'))) {
      try {
        sendDeleteNoteRequest(note.id)
      } catch {
        setError(t('networkError'))
      }
    }
  }

  return (
    <StickyNote key={note.id}>
      <Content direction="column">
        <span className="text-truncate">{note.title}</span>
      </Content>

      <div className="d-flex flex-row justify-content-between">
        <NeutralButton label={t('show')} onClick={showNote} />
        <NegativeButton label={t('delete')} onClick={deleteNote} />
      </div>
    </StickyNote>
  )
}

export default withRouter(
  connect(
    null,
    mapDispatch
  )(NoteControl)
)
