import { ReactNode } from 'react'

export interface ChildrenRenderingComponent {
  children?: ReactNode
}
