import * as React from 'react'
import { FlagProps } from '.'

const CzechFlag = ({ onClick, className }: FlagProps) => (
  <svg
    className={className}
    onClick={onClick}
    xmlns="http://www.w3.org/2000/svg"
    version="1.0"
    width="23"
    height="16"
  >
    <rect width="23" height="16" fill="#d7141a" />
    <rect width="23" height="8" fill="#fff" />
    <path d="M 16,8 0,0 V 16 z" fill="#11457e" />
  </svg>
)

export default CzechFlag
