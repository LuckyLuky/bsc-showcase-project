import * as React from 'react'
import { ChildrenRenderingComponent } from '..'
import './StickyIcon.scss'

interface StickyIconProps extends ChildrenRenderingComponent {}

const StickyIcon = ({ children }: StickyIconProps) => (
  <div className="icon--sticky">{children}</div>
)

export default StickyIcon
