import * as React from 'react'
import './Spinner.scss'

const Spinner = () => <i className="fas fa-spinner icon--spinner" />

export default Spinner
