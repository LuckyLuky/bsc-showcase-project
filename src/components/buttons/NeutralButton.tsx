import * as React from 'react'
import { ButtonProps } from '.'
import './NeutralButton.scss'

const NeutralButton = ({ label, onClick }: ButtonProps) => (
  <button onClick={onClick} type="button" className="button--neutral">
    {label}
  </button>
)

export default NeutralButton
