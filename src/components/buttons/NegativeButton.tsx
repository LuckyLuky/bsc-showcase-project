import * as React from 'react'
import { ButtonProps } from '.'
import './NegativeButton.scss'

const NegativeButton = ({ label, onClick }: ButtonProps) => (
  <button onClick={onClick} type="button" className="button--negative">
    {label}
  </button>
)

export default NegativeButton
