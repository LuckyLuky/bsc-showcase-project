import notes, { NotesState } from './notes'
import errors from './errors'
import language from './language'
import { combineReducers, createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'

const reducers = combineReducers({
  notes,
  errors,
  language,
})

const store = createStore(reducers, applyMiddleware(reduxThunk))

export type Store = typeof store
export type State = ReturnType<typeof store['getState']>

export default store
