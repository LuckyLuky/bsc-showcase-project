/**
 * State
 */
export interface ErrorsState {
  networkErrors: string[]
}
const defaultState: ErrorsState = {
  networkErrors: [],
}

/**
 * Action types
 */
const SET_NETWORK = 'notie/errors/set-network'

/**
 * Reducer
 */
export default function reducer(
  state = defaultState,
  action: ErrorsActionCreator
) {
  switch (action.type) {
    case SET_NETWORK:
      const { networkErrors } = <SetNetworkErrorsActionCreator>action

      return {
        ...state,
        networkErrors,
      }

    default:
      return state
  }
}

/**
 * Action creators
 */
export const setNetworkErrors = (networkErrors: string[]) => ({
  networkErrors,
  type: SET_NETWORK,
})
type SetNetworkErrorsActionCreator = ReturnType<typeof setNetworkErrors>

export type ErrorsActionCreator = SetNetworkErrorsActionCreator
