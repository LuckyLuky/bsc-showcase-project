import { Dispatch } from 'redux'
import { sendGetAllNotesRequest, sendGetNoteRequest } from '../api/notes'
import { setNetworkErrors } from './errors'
import { Store } from '.'
import i18n from '../i18n'

/**
 * Typings
 */
export interface Note {
  id: number
  title: string
}

/**
 * State
 */
export interface NotesState {
  notes: Note[]
  activeNote: Note | null
}
const defaultState: NotesState = {
  notes: [],
  activeNote: null,
}

/**
 * Action types
 */
const SET_ALL = 'notie/notes/set-all'
const SET_ACTIVE = 'notie/notes/set-active'

/**
 * Reducer
 */
export default function reducer(
  state = defaultState,
  action: NotesActionCreator
) {
  switch (action.type) {
    case SET_ALL:
      const { notes } = <SetNotesActionCreator>action

      return {
        ...state,
        notes,
      }

    case SET_ACTIVE:
      const { note } = <SetActiveNoteActionCreator>action

      return {
        ...state,
        activeNote: note,
      }

    default:
      return state
  }
}

/**
 * Action creators
 */
export const setNotes = (notes: Note[]) => ({
  notes,
  type: SET_ALL,
})
type SetNotesActionCreator = ReturnType<typeof setNotes>

export const setActiveNote = (note: Note | null) => ({
  note,
  type: SET_ACTIVE,
})
type SetActiveNoteActionCreator = ReturnType<typeof setActiveNote>

export type NotesActionCreator =
  | SetNotesActionCreator
  | SetActiveNoteActionCreator

/**
 * Side effects
 */
export const loadNotes = () => async (dispatch: Dispatch) => {
  try {
    const notes = await sendGetAllNotesRequest()
    dispatch(setNotes(notes))
  } catch {
    dispatch(setNetworkErrors([i18n.t('networkError')]))
  }
}

export const loadNote = (noteId: Note['id']) => async (dispatch: Dispatch) => {
  try {
    const note = await sendGetNoteRequest(noteId)
    dispatch(setActiveNote(note))
  } catch {
    dispatch(setNetworkErrors([i18n.t('networkError')]))
  }
}
