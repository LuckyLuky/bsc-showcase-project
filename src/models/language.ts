/**
 * Typings
 */
export enum Language {
  CS = 'cs',
  EN = 'en',
}

/**
 * State
 */
export type LanguageState = Language
const defaultState: LanguageState = Language.CS

/**
 * Action types
 */
const SET = 'notie/language/set'

/**
 * Reducer
 */
export default function reducer(
  state = defaultState,
  action: LanguageActionCreator
) {
  switch (action.type) {
    case SET:
      const { language } = <SetLanguageActionCreator>action

      return language

    default:
      return state
  }
}

/**
 * Action creators
 */
export const setLanguage = (language: Language) => ({
  language,
  type: SET,
})
type SetLanguageActionCreator = ReturnType<typeof setLanguage>

export type LanguageActionCreator = SetLanguageActionCreator
