import * as React from 'react'
import { connect } from 'react-redux'
import HomeLayout from '../components/layout/HomeLayout'
import ErrorBar from '../components/ui/ErrorBar'
import StickyNote from '../components/ui/StickyNote'
import { State } from '../models'
import { Dispatch } from 'redux'
import { loadNotes, NotesActionCreator, Note } from '../models/notes'
import { ThunkDispatch } from 'redux-thunk'
import Content from '../components/layout/Content'
import NotesList from '../components/ui/NotesList'
import Description from '../components/text/Description'
import { useTranslation } from 'react-i18next/hooks'
import Spinner from '../components/icons/Spinner'
import StickyIcon from '../components/icons/StickyIcon'
import { Link } from 'react-router-dom'
import { AppRoutes } from '../AppRouter'

const mapState = (state: State) => ({
  notes: state.notes.notes,
})

const mapDispatch = (
  dispatch: ThunkDispatch<State, null, NotesActionCreator>
) => ({
  loadNotes: () => dispatch(loadNotes()),
})

type NotesListScreenProps = ReturnType<typeof mapState> &
  ReturnType<typeof mapDispatch>

const NotesListScreen = ({ notes, loadNotes }: NotesListScreenProps) => {
  const [t] = useTranslation()

  const [isLoading, setIsLoading] = React.useState(true)

  React.useEffect(() => {
    // Types for useEffect don't support async function, that's why we use callback
    loadNotes().then(() => {
      setIsLoading(false)
    })
  }, [])

  return (
    <HomeLayout>
      <Content>
        <Description>{t('description')}</Description>
      </Content>

      <Content wrap centered>
        {isLoading ? <Spinner /> : <NotesList notes={notes} />}
      </Content>

      <Link to={AppRoutes.AddNote}>
        <StickyIcon>&#x2b;</StickyIcon>
      </Link>
    </HomeLayout>
  )
}

export default connect(
  mapState,
  mapDispatch
)(NotesListScreen)
