import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { sendUpdateNoteRequest, sendCreateNoteRequest } from '../api/notes'
import { connect } from 'react-redux'
import { Note } from '../models/notes'
import NoteForm from '../components/forms/NoteForm'
import HomeLayout from '../components/layout/HomeLayout'
import Content from '../components/layout/Content'
import { setNetworkErrors } from '../models/errors'
import { useTranslation } from 'react-i18next/hooks'
import { Dispatch } from 'redux'

const mapDispatch = (dispatch: Dispatch) => ({
  setError: (error: string) => dispatch(setNetworkErrors([error])),
})

interface Params {
  id: string
}

type NoteDetailScreenProps = RouteComponentProps<Params> &
  ReturnType<typeof mapDispatch>

const NoteDetailScreen = ({ history, setError }: NoteDetailScreenProps) => {
  const [t] = useTranslation()

  const onFormSubmit = (note: Note) => {
    try {
      sendCreateNoteRequest(note)
      history.goBack()
    } catch {
      setError(t('networkError'))
    }
  }

  const onFormCancel = () => {
    history.goBack()
  }

  return (
    <HomeLayout>
      <Content centered>
        <NoteForm onSubmit={onFormSubmit} onCancel={onFormCancel} />
      </Content>
    </HomeLayout>
  )
}

export default connect(
  null,
  mapDispatch
)(NoteDetailScreen)
