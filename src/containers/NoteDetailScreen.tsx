import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { sendGetNoteRequest, sendUpdateNoteRequest } from '../api/notes'
import { connect } from 'react-redux'
import { ThunkDispatch } from 'redux-thunk'
import { State } from '../models'
import {
  loadNote,
  NotesActionCreator,
  Note,
  setActiveNote,
} from '../models/notes'
import Spinner from '../components/icons/Spinner'
import NoteForm from '../components/forms/NoteForm'
import HomeLayout from '../components/layout/HomeLayout'
import Content from '../components/layout/Content'
import { ErrorsActionCreator, setNetworkErrors } from '../models/errors'
import { useTranslation } from 'react-i18next/hooks'

const mapState = (state: State) => ({
  note: state.notes.activeNote,
})

const mapDispatch = (
  dispatch: ThunkDispatch<State, null, NotesActionCreator | ErrorsActionCreator>
) => ({
  loadNote: (noteId: Note['id']) => dispatch(loadNote(noteId)),
  clearActiveNote: () => dispatch(setActiveNote(null)),
  setError: (error: string) => dispatch(setNetworkErrors([error])),
})

interface Params {
  id: string
}

type NoteDetailScreenProps = RouteComponentProps<Params> &
  ReturnType<typeof mapState> &
  ReturnType<typeof mapDispatch>

const NoteDetailScreen = ({
  match,
  history,
  note,
  loadNote,
  clearActiveNote,
  setError,
}: NoteDetailScreenProps) => {
  const [t] = useTranslation()

  const [isLoading, setIsLoading] = React.useState(true)

  const noteId = match.params.id

  React.useEffect(() => {
    // Types for useEffect don't support async function, that's why we use callback
    loadNote(Number(noteId)).then(() => {
      setIsLoading(false)
    })
  }, [])

  const onFormSubmit = (note: Note) => {
    try {
      sendUpdateNoteRequest(note)
      clearActiveNote()
      history.goBack()
    } catch {
      setError(t('networkError'))
    }
  }

  const onFormCancel = () => {
    history.goBack()
  }

  return (
    <HomeLayout>
      <Content centered>
        {isLoading ? (
          <Spinner />
        ) : (
          <NoteForm
            note={note}
            onSubmit={onFormSubmit}
            onCancel={onFormCancel}
          />
        )}
      </Content>
    </HomeLayout>
  )
}

export default connect(
  mapState,
  mapDispatch
)(NoteDetailScreen)
