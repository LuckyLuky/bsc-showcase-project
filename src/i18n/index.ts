import i18n from 'i18next'
import { reactI18nextModule } from 'react-i18next'
import { initReactI18n } from 'react-i18next/hooks'
import LanguageDetector from 'i18next-browser-languagedetector'
import enTranslations from './translations/en.json'
import csTranslations from './translations/cs.json'
import { getLanguage } from '../tools/local-storage'

const language = getLanguage() || 'cs'

i18n
  .use(reactI18nextModule)
  .use(LanguageDetector)
  .use(initReactI18n)
  .init({
    debug: process.env.NODE_ENV === 'development',
    resources: {
      en: {
        translation: {
          ...enTranslations,
        },
      },
      cs: {
        translation: {
          ...csTranslations,
        },
      },
    },
    lng: language,
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false,
    },
    react: {
      wait: true,
    },
  })

export default i18n
