import { BASE_URL } from '.'
import { Note } from '../models/notes'

type GetAllNotesResponse = Note[]
const GET_ALL_NOTES_URL = `${BASE_URL}/notes`
export const sendGetAllNotesRequest = async (): Promise<
  GetAllNotesResponse
> => {
  const response = await fetch(GET_ALL_NOTES_URL)

  const result: GetAllNotesResponse = await response.json()

  return result
}

const DELETE_NOTE_URL = `${BASE_URL}/note`
export const sendDeleteNoteRequest = (noteId: Note['id']): void => {
  fetch(`${DELETE_NOTE_URL}/${noteId}`, {
    method: 'DELETE',
  })
}

type GetNoteResponse = Note
const GET_NOTE_URL = `${BASE_URL}/notes`
export const sendGetNoteRequest = async (
  noteId: Note['id']
): Promise<GetNoteResponse> => {
  const response = await fetch(`${GET_NOTE_URL}/${noteId}`)

  const result: GetNoteResponse = await response.json()

  return result
}

const UPDATE_NOTE_URL = `${BASE_URL}/notes`
export const sendUpdateNoteRequest = (note: Note): void => {
  fetch(`${UPDATE_NOTE_URL}/${note.id}`, {
    method: 'PUT',
    body: JSON.stringify(note),
  })
}

const CREATE_NOTE_URL = `${BASE_URL}/notes`
export const sendCreateNoteRequest = (note: Note): void => {
  fetch(CREATE_NOTE_URL, {
    method: 'POST',
    body: JSON.stringify(note),
  })
}
