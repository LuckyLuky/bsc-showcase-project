import React, { Component } from 'react'
import './App.scss'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import NotesListScreen from './containers/NotesListScreen'
import NoteDetailScreen from './containers/NoteDetailScreen'
import AddNoteScreen from './containers/AddNoteScreen'
import ErrorScreen from './components/ui/ErrorScreen'

export enum AppRoutes {
  Default = '/',
  NoteDetail = '/note',
  AddNote = '/new-note',
}

class AppRouter extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path={AppRoutes.Default} component={NotesListScreen} />
          <Route
            exact
            path={`${AppRoutes.NoteDetail}/:id`}
            component={NoteDetailScreen}
          />
          <Route exact path={AppRoutes.AddNote} component={AddNoteScreen} />

          <Route component={ErrorScreen} />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter
