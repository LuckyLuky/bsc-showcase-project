## Project setup

(_Required only for first setup:_) Run `yarn install` in the root directory to install dependencies.

Run `yarn start` to start development server and load web page.

Homepage should automatically load at http://localhost:3000/.

## Production build

If you want to create a production build, run `yarn build` in the root directory. It should create `build/` directory containing all the necessary files.

## Tests

### End to end

Run `yarn update-webdriver` to download necessary binaries and `yarn start-webdriver` to start Selenium Server.

Run `yarn test-protractor` to run end to end tests using _Protractor_. _(Note: Selenium Server has to be up and running.)_
